import { StatusBar } from 'expo-status-bar';
import React, { useEffect } from 'react';
import {useState} from 'react';
import { StyleSheet, Text, View,Button,Platform,ActivityIndicator,FlatList} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';


export default function SecondScreen({navigation}:{navigation:any}) {

    const[isLoading,setLoading]=useState(true);
    const[data,setData]=useState([]);

    const getMovies=async()=>{

        try
        {
                  const reponse=await fetch('https://reactnative.dev/movies.json');

                  const json=await reponse.json();

                  setData(json.movies);
        }
        catch(error)
        {
            console.error(error);
        }finally{

            setLoading(false);
        }

    }

    useEffect(()=>{

        getMovies();
    },[]);

    const item=({title:any})=>(

        <View style={styles.item}>

       <Text style={styles.title}> {title}</Text>

   </View>
    );

  return (

   
    <View style={styles.container}>

      <Text style={styles.btn} >Second Screen</Text>

      {isLoading?<ActivityIndicator/>:(

          <FlatList
          data={data}
          keyExtractor={({id},index) =>id}
          renderItem={({item})=>(


            <View style={styles.item}>
            <Text style={{color:'red'}}>
                 {item.title} 
            </Text>

             <Text >
                 {item.releaseYear}  
            </Text>
            </View>
          )}
          />
      )}
      <Button title="Fetch Data from API" 
      onPress={()=>{

            getMovies();
        }}
        />

      <Button title="go to First screen" 
      onPress={()=>{

            navigation.navigate('FirstScreen',{itemid:86,RestoName:"PaneraBread",});
        }}
        />


      <StatusBar style="auto" />
    </View>
   
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
   
  },
  btn:{

    height:Platform.OS==='android'?400:200,
    fontSize:Platform.OS==='web'?50:5,

  },
   title:{
        
    fontSize:32,
   },

   item:{

    backgroundColor:'#f9c2ff',
    padding:20,
    marginVertical:8,
    marginHorizontal:16,
    fontSize:32,
   }

});
