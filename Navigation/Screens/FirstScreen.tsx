import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import {NavigationContainer} from '@react-navigation/native';

export default function FirstScreen({navigation,route}) {

    const {itemid,RestoName}=route.params;

  return (

    
    <View style={styles.container}>
      <Text>First Screen</Text>
      <Text> RestoID: {JSON.stringify(itemid)}</Text>

      <Button title="go to second screen" onPress={()=>{

          navigation.push('SecondScreen')}
          }/>

<Button title="go Back" onPress={()=>{

navigation.goBack()}
}/>

<Button title="Go to to Top of the Stack" onPress={()=>{

navigation.popToTop()}
}/>

      <StatusBar style="auto" />
    </View>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
