import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack'
import FirstScreen from './Screens/FirstScreen';
import SecondScreen from './Screens/SecondScreen';


const Stack=createNativeStackNavigator();

export default function App() {
  return (

    <NavigationContainer>

      <Stack.Navigator initialRouteName="SecondScreen" 
      screenOptions= {{headerStyle:{

        backgroundColor:'#f4511e',},headerTintColor:'#fff'}}
      >

        <Stack.Screen

        name="FirstScreen"
        component={FirstScreen}
       />

         <Stack.Screen

        name="SecondScreen"
        component={SecondScreen}
        options={{title:"SecondScreen"
       
        }}
        initialParams={{itemID:42}}
       />

    

      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
