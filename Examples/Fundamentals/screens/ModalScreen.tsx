import { StatusBar } from 'expo-status-bar';
import * as React from 'react';
import { Platform, StyleSheet, TouchableOpacity } from 'react-native';
import { useState } from 'react';
import {Image} from 'react-native'

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import { ImageComp } from './ImageComp';

  

const ModalScreen=()=> {

  const[direction,setDirection]=useState('ltr');

  return (

   
     
    <PreviewLayout label="direction"
     selectedValue={direction} 
     values={["ltr","rtl"]} 
     setSelectedValue={setDirection}

    >
      
      <View style={[styles.box,{backgroundColor:"red"}]}/>
      <ImageComp></ImageComp>

     

      <View style={[styles.box,{backgroundColor:"blue"}]}/>

      <View style={[styles.box,{backgroundColor:"green"}]}/>


    </PreviewLayout>


   
  );
};

interface parametersPL {
  label:any,
  children:any,
  values:any,
  selectedValue:any,
  setSelectedValue:any
}

const PreviewLayout = (parameters:parametersPL)=>(<>


  <View style={{padding:10,flex:1}}>


<Text style={styles.label}>{parameters.label}</Text>

<View style={styles.row}>

  {parameters.values.map((value: any)=>(

  <TouchableOpacity key={value}
      onPress={() => parameters.setSelectedValue(value)}
      style={[styles.button, parameters.selectedValue === value && styles.selected,]}> 
      
      <Text style={[styles.buttonLabel, parameters.selectedValue === value && styles.selectedLable,]}>{value}</Text>
      </TouchableOpacity>
))}
</View>

<View style={[styles.container,{[parameters.label]:parameters.selectedValue}]}>

{parameters.children}

</View>

</View>
</>

)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:8,
    backgroundColor:"aliceblue",
  },
  box:{
    width:50,
    height:50,
  },
  row:{

    flexDirection:"row",
    flexWrap:"wrap",

  },
  button:{

    paddingHorizontal:8,
    paddingVertical:6,
    borderRadius:4,
    backgroundColor:"oldlace",
    marginHorizontal:"1%",
    marginBottom:6,
    minWidth:"48%",
    textAlign:"center",
  },
  selected:{

    backgroundColor:"coral",
    borderWidth:0,
  },

  buttonLabel:{
    fontSize:12,
    fontWeight:"500",
    color:"coral",
  },

  label:{

    textAlign:"center",
    marginButton:10,
    fontSize:24,

  },

  selectedLable:{

    color:"white",
  }

 
});

export default ModalScreen;
