import React, { useState } from 'react';
import { Button, StyleSheet} from 'react-native';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import { RootTabScreenProps } from '../types';
export default function HelloWorld({ navigation }: RootTabScreenProps<'TabOne'>) 
{

  return(

    <View style={{
        flex:1,
        justifyContent:"center",
        alignItems:"center",
    }}>

     <UsingState></UsingState>

     <Greetings name="Mayur"/>
     <Greetings name="Charles"/>
     <Greetings name="Nick"/>
     <Greetings name="Ahmed"/>


    </View>
  )
}

const UsingState=()=>{

  const[count,setCount]=useState(0);


  return(

    <View>

      <Text>You clicked {count} times</Text>

      <Button onPress={()=>setCount(count+1)} title="Click Me">

      </Button>

    </View>
  )


}

const Greetings=(props:any)=>
{
  return(
  <View style={{top:50}}>
    <Text>
      Hello,{props.name}!
    </Text>
  </View>

)
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
