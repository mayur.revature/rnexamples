import * as React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Component } from 'react';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';

export const FixedDimentionBasics=()=>
{

  return(

  <View style={{flex:1, flexDirection:"column",justifyContent:"center"}}>

{/*   
<View style={{width:50,height:50,backgroundColor:'powerderblue'}}/>

<View style={{width:100,height:100,backgroundColor:'skyblue'}}/>  

<View style={{width:150,height:150,backgroundColor:'steelblue'}}/> */}

  
<View style={{flex:1,backgroundColor:'red'}}>View 1</View>

<View style={{flex:2,backgroundColor:'darkorange'}}>  View 2</View>

<View style={{flex:3,backgroundColor:'green'}}>View 3</View>

1+2+3=6

1/6
2/6
3/6

</View>
  )

}


export class TabTwoScreenClass extends React.Component<{name:any}>
{


  constructor(props:any)
  {
    super(props);
  }
  state={

    count:0
  }

  onPress=()=>{

    this.setState({count:this.state.count+1})
  }

  render()
  {

    return(

        
     
      <View style={styles.container}>

    



        <TouchableOpacity  onPress={this.onPress}>

       

       <Text style={styles.title}>Click Me</Text>

       </TouchableOpacity>

       <View>
         <Text>
          You clicked {this.state.count} times! {this.props.name}

         </Text>

       
       </View>

      </View>
    )
  }


}

export default function TabTwoScreen() {
  return (

    
    <View style={styles.container}>


<FixedDimentionBasics/>

<TabTwoScreenClass name="Hello Class Props">

      
</TabTwoScreenClass>


      
      <Text >Tab 2</Text>
      <View  lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />

     
    

     
    </View>
  );
}

const styles=StyleSheet.create({

container:{


  padding:24,
  backgroundColor:"#eaeaea"
},

title:{

  backgroundColor:"#61dafb",
  color:"20232a",
  fontSize:30,
  fontWeight:"bold",
  borderWidth:4,
  borderColor:"#23232a",


}

})

